/**
* Created by frobinet on 25/11/15.
*/

import java.io.{FileReader, BufferedReader}
import java.util.Scanner

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class PSPModel(private val ni: Int, private val np: Int, private val nt: Int, private val h: Int,
               types: Array[Int], deadlines: Array[Int],
               private val deadlinesSet: Array[mutable.HashSet[Int]],
               changeOverCosts: Array[Array[Int]]) {

  // Add dummy item with index ni, type nt, deadline np
  val t = Array.tabulate(ni+1) { i => if(i == ni) nt else types(i) }
  val d = Array.tabulate(ni+1) { i => if(i == ni) np else deadlines(i) }
  val Q = Array.tabulate(nt+1,nt+1) { (i,j) => {
        if(i == nt || j == nt) 0
        else changeOverCosts(i)(j)
     }
  }

  /**
   * Places item on the closest free spot (occupied by a dummy item) before index i in array p
   */
  private def placeClosest(p: Array[Int], i: Int, item: Int): Unit = {
    var j = i
    while(j >= 0) {
      if(p(j) == ni) { // Found dummy item
        p(j) = item
        return
      }
      j -= 1
    }
    throw new NoSuchElementException("Can't place item %d before index %d in array %s".format(item, i, p.deep))
  }

  private def swap(p: Array[Int], posj: Int, posk: Int) = {
    val j = p(posj)
    p(posj) = p(posk)
    p(posk) = j
  }

  private def permutationCost(perm: Array[Int], lastProduced: Int) = {
    var cost = Q(lastProduced)(t(perm(0)))
    for(i <- 1 until perm.length)
      cost += Q(t(perm(i-1)))(t(perm(i)))
    cost
  }

  private def findBestPermutation(indices: mutable.HashSet[Int], lastProduced: Int): Array[Int] = {
    val array = indices.toArray
    var bestPermutation: Array[Int] = null
    var bestCost = Int.MaxValue
    for(perm <- array.permutations) {
      val cost = permutationCost(perm, lastProduced)
      if(cost < bestCost) {
        bestPermutation = perm
        bestCost = cost
      }
    }
    bestPermutation
  }

  private def findBestPermutationLocalSearch(indices: mutable.HashSet[Int], lastProduced: Int): Array[Int] = {
    val bestPermutation: Array[Int] = indices.toArray
    var bestCost = Int.MaxValue
    var swapPossible = true
    while(swapPossible) {
      swapPossible = false
      for (i <- 0 until bestPermutation.length; j <- i + 1 until bestPermutation.length) {
        swap(bestPermutation, i, j)
        val c = permutationCost(bestPermutation, lastProduced)
        if(c < bestCost) {
          swapPossible = true
          bestCost = c
        } else {
          swap(bestPermutation, i, j)
        }
      }
    }
    bestPermutation
  }

  private def findInitialSolution() = {
    var lastProduced = nt
    val p = Array.fill(np) { ni }
    for (i <- 0 until np) {
      val items = deadlinesSet(i)
      if(items.nonEmpty) {

        // Find best permutation in terms of cost
        var perm: Array[Int] = null
        if(items.size > 6) {
          // Too many types at deadline to find best permutation -> use local search
          perm = findBestPermutationLocalSearch(items, lastProduced)
        } else {
          perm = findBestPermutation(items, lastProduced)
        }

        // Place the element in order
        for(j <- perm.length-1 to 0 by -1)
          placeClosest(p, i, perm(j)) // Place perm(j) before index i in p (last placed first)

        lastProduced = t(perm.last)
      }
    }
    p
  }

  private def delta(t: Long) = System.currentTimeMillis()-t

  private def cost(p: Array[Int]): Int = {
    val first = p(0)
    var lastProducedKind = t(first)
    var storage = if(first < ni) d(first) else 0
    var c = 0
    for(j <- 1 until p.length) {
      val i = p(j)
      if(i < ni) { // Not a dummy item -> lastProduced should change and there are storage/changeover costs
      val kind = t(i)
        c += Q(lastProducedKind)(kind)
        storage += (d(i) - j)
        lastProducedKind = kind
      }
    }
    c + h * storage
  }

  def solve(time:Long = 10000): (Array[Int], Int) = {
    // Find initial Solution
    val start = System.currentTimeMillis()
    val firstSol = findInitialSolution()
    val firstCost = cost(firstSol)

    // Improve this solution for the rest of the time
    solve(firstSol, firstCost, time-delta(start))
  }

  def solve(sol: Array[Int], initCost: Int, time: Long): (Array[Int], Int) = {

    val start = System.currentTimeMillis()
    val rand = new Random()

    // Current Solution
    var solCost = initCost // Current sol cost

    // Best solution
    val bestSol = Array.ofDim[Int](np) // best solution overall (even after restarts)
    Array.copy(sol, 0, bestSol, 0, sol.length)
    var bestCost = initCost // best sol cost

    // Tabu and swap structures
    val tabu = Array.fill[Long](np,np) { 0L }
    val allSwaps = for(i <- 0 until np; j <- i+1 until np) yield (i,j)
    var iterations = 0L
    var bestSwap = (0,0)

    // Search parameters
    val tabuTenure = 40
    while(delta(start) < time) {
      var bestSwapCost = Int.MaxValue
      iterations += 1
      var moveExists = false

      for ((i,j) <- allSwaps) {
        val (a,b) = (sol(i), sol(j))

        /*
         * A swap is correct if:
         *    - it respects the deadline for j (since j < k, deadline for k will be respected too)
         *    - types swapped are different
         */
        if (j <= d(a) && t(a) != t(b)) {
          //assert(i <= d(b)) // Should also be correct
          swap(sol, i, j)
          val c = cost(sol)

          // Aspiration: make move even if tabu when it leads to best solution ever
          if (tabu(i)(j) < iterations || c < bestCost) {
            moveExists = true

            // This does the best move, even if not a good one!
            if (c < bestSwapCost) {
              bestSwap = (i, j)
              bestSwapCost = c
            }
          }

          // All moves for which we computed the cost become tabu
          tabu(i)(j) = iterations + rand.nextInt(tabuTenure)

          // Unswap
          swap(sol, i, j)
        }
      }

      if (moveExists) {
        swap(sol, bestSwap._1, bestSwap._2)
        solCost = bestSwapCost
        if (solCost < bestCost) { // Keep best solution
          bestCost = solCost
          Array.copy(sol, 0, bestSol, 0, sol.length)
        }
      }
    }

    //assert(cost(bestSol) == bestCost)
    //for(i <- 0 until np)
      //assert(i <= d(sol(i)))
    (bestSol, bestCost)
  }
}




object PSPModel extends App {

  /*
    Read input file
  */
  val filename = if (args.length > 0) args(0) else "instances/pspA.txt"
  val sc = new Scanner(new BufferedReader(new FileReader(filename)))

  val (np, nt) = (sc.next.toInt, sc.next.toInt)
  val t = ArrayBuffer[Int]() // t(i) = type of item i
  val d = ArrayBuffer[Int]() // d(i) = deadline for item i
  val deadlines = Array.fill(np) { new mutable.HashSet[Int]() }
  for (i <- 0 until nt; j <- 0 until np) {
    val b = sc.next.toInt
    if (b == 1) {
      t += i
      d += j
      deadlines(j) += t.length-1 // index for each deadline
    }
  }

  val h = sc.next.toInt
  val ni = t.size
  val changeOverCosts = Array.tabulate(nt, nt) { (i,j) => sc.next.toInt }
  sc.close()

  val model = new PSPModel(ni, np, nt, h, t.toArray, d.toArray, deadlines, changeOverCosts)
  val (bestSol, cost) = model.solve(270000) // 4 min 40 to solve
  val indexSolution = bestSol map { i => if (i < ni) t(i) else -1 }
  println(cost)
  println(indexSolution.mkString(" "))


}
