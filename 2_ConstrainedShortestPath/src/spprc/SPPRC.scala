package spprc

import java.io.{BufferedReader, FileReader}
import java.util.Scanner

import graph.UndirectedStaticGraph
import graph.UndirectedStaticGraph.Types.{Edge, Node, Time }
import graph.search.SearchNode

import scala.collection.mutable

class SPPRC(val graph: UndirectedStaticGraph, val source: Node, val target: Node, val T: Time) {

  private var bestPath: List[SPPRCState] = Nil
  private var bestWeight = Int.MaxValue

  def solve() = {
    // Find greedy solution first using a shortest path that only considers times
    val root = new SearchNode[SPPRCState](None, SPPRCState(source, 0, 0))
    val (greedyState,greedyPath) = graph.shortestPathWithPath(target, { case(w,t) => t }, root)
    bestWeight = greedyState.pathWeight
    bestPath = greedyPath

    // Branch and Bound search
    val searchStack = mutable.Stack[SearchNode[SPPRCState]]()
    searchStack.push(root)
    while(searchStack.nonEmpty) {
      val searchNode = searchStack.pop()
      val state = searchNode.state
      if(!reject(searchNode)) {
        if (accept(state)) {
          output(searchNode.pathToRoot)
        } else {
          val pathNodes = searchNode.pathToRoot.map(_.node).toSet
          val successors = graph.successors(state.node)
          for ((n,(w,t)) <- successors) {
            if(!pathNodes.contains(n)) {
              val nextState = SPPRCState(n, w + state.pathWeight, t + state.pathTime)
              val nextNode = new SearchNode(Option(searchNode), nextState)
              searchStack.push(nextNode)
            }
          }
        }
      }
    }
  }

  private[this] def accept(state: SPPRCState): Boolean = {
    state.node == target && state.pathTime <= T && state.pathWeight < bestWeight
  }

  private[this] def reject(searchNode: SearchNode[SPPRCState]): Boolean = {
    val state = searchNode.state
    if(state.pathTime <= T) {
      val lagrangian = computeLagrangian(searchNode)
      val LB = binarySearch(lagrangian, 0, 100, 0.01, bestWeight)._1.ceil.toInt
      bestWeight <= LB
    }
    else
      true
  }

  private[this] def output(path: List[SPPRCState]) = {
    bestWeight = path.last.pathWeight
    bestPath = path
    //println("Solution (w=%d,t=%d): %s" format (bestWeight, path.last.pathTime, bestPath))
  }

  private[this] def computeLagrangian(searchNode: SearchNode[SPPRCState]) = {
    (lambda: Double) => {
      val cost = (e:Edge) => e._1 + lambda * (e._2-T)
      val state = graph.shortestPath(target, cost, searchNode.state)
      if(accept(state)) {
        val (_, path) = graph.shortestPathWithPath(target, cost, searchNode) // Recompute the path
        output(path)
      }
      cost(state.pathWeight, state.pathTime)
    }
  }


  private[this] def binarySearch(f: Double => Double , a: Double, b: Double, epsilon: Double, toBeat: Double): (Double,Double) = {
    var (left,right) = (a,b)
    var fleft = f(left)
    var fright = f(right)
    var slack = (fright-fleft).abs

    while(slack > epsilon) {
      val median = (left+right)/2

      if(fleft > toBeat && fright > toBeat) {
        // Can't hope to beat current solution, no need to keep optimizing
        return ((fleft+fright)/2, median)
      }

      if(fright < fleft) {
        right = median
        fright = f(right)
      } else {
        left = median
        fleft = f(left)
      }

      slack = (fright-fleft).abs
    }

    ((fleft+fright)/2, (left+right)/2)

  }

}



object SPPRC extends App {
  // Read input file
  val filename = if(args.length > 0) args(0) else "spprcC.txt"
  val content = mutable.Queue[Int]()
  val sc = new Scanner(new BufferedReader(new FileReader(filename)))
  val (n,m,source,target) = (sc.next.toInt,sc.next.toInt,sc.next.toInt,sc.next.toInt)
  val T = sc.next.toInt
  for(i <- 0 to n) sc.nextLine()
  var edges: List[(Int,Int,Int,Int)] = Nil
  for(i <- 0 until m)
    edges = (sc.next.toInt, sc.next.toInt, sc.next.toInt, sc.next.toInt) :: edges
  sc.close()

  // Solve problem
  val G = new UndirectedStaticGraph(n, edges)
  val problem = new SPPRC(G, source, target, T)
  val t = System.currentTimeMillis()
  problem.solve()
  println("Ellapsed: " + (System.currentTimeMillis()-t) + " (ms)")

  // Output Solution
  println(problem.bestWeight)
  println(problem.bestPath.map(_.node).mkString(" "))
}


