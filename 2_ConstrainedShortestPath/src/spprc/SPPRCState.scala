package spprc

import graph.UndirectedStaticGraph.Types._

class SPPRCState(val node: Node, val pathWeight: Int, val pathTime: Int) {

  override def toString =
    "(node=%d, totalWeight=%d, totalTime=%d)" format (node, pathWeight, pathTime)

  def equals(that: SPPRCState) = {
    that.node == node && that.pathWeight == pathWeight && that.pathTime == pathTime
  }
}

object SPPRCState {
  def apply(node: Node, pathWeight: Int, pathTime: Int) =
    new SPPRCState(node, pathWeight, pathTime)
}