package graph

import graph.UndirectedStaticGraph.Types.{Weight, Edge, Node, Time}
import graph.search.SearchNode
import spprc.SPPRCState

import scala.collection.mutable

class UndirectedStaticGraph(private val V: Int, private[this] val edgesList: List[(Node,Node,Weight,Time)]) {

  val E = edgesList.length
  private[this] val nodes = Array.ofDim[Node](V+1) // 1 dummy entry at the end
  private[this] val edges = Array.ofDim[Edge](E)

  private[this] val mapping: Array[(Node,Int)] = {
    val edgesSets = Array.fill[mutable.HashSet[(Node,Int)]](V){ mutable.HashSet[(Node,Int)]() }
    for(i <- edgesList.indices) {
      val (a,b,w,t) = edgesList(i)
      edges(i) = (w,t) // Create edge (w,t) at index i
      edgesSets(a) add b ->i // Add edge i from a to b
      edgesSets(b) add a -> i // Add edge i from b to a
    }

    val map = Array.ofDim[(Node,Int)](2*E)
    var i = 0
    for(j <- edgesSets.indices) {
      nodes(j) = i
      for(e <- edgesSets(j)) {
        map(i) = e
        i += 1
      }
    }

    nodes(V) = 2*E // Dummy pointer
    map
  }

  def checkNode(n: Node) =
    if(n < 0 || n > nodes.length)
      throw new IllegalArgumentException("Node " + n + " isn't in the graph")

  def successors(n: Node): Seq[(Node, Edge)] = {
    (nodes(n) until nodes(n+1)).map(i => {
      val (m, j) = mapping(i)
      (m, edges(j))
    })
  }

  private[this] val explored = new mutable.HashSet[Node]()

  def shortestPathWithPath(to: Node, cost: (Edge => Double), init: SearchNode[SPPRCState]): (SPPRCState, List[SPPRCState]) = {
    checkNode(to)
    val ord = Ordering.by((n: SearchNode[SPPRCState]) => -cost(n.state.pathWeight, n.state.pathTime))
    val fringe = new mutable.PriorityQueue[SearchNode[SPPRCState]]()(ord)
    explored.clear() // Avoids rebuilding explored on each call

    fringe += init

    while(fringe.nonEmpty) {
      val searchNode = fringe.dequeue()
      val state = searchNode.state
      val n = state.node
      if(n == to)
        return (state, searchNode.pathToRoot)
      explored += n
      for ((m, (w,t)) <- successors(n))
        if(!explored.contains(m))
          fringe enqueue new SearchNode(Some(searchNode), SPPRCState(m, state.pathWeight+w, state.pathTime+t))
    }

    throw new IllegalArgumentException("Can't reach node " + to + " from node " + init.state.node)
  }

  def shortestPath(to: Node, cost: (Edge => Double), init: SPPRCState): SPPRCState = {
    checkNode(to)
    val ord = Ordering.by((state: SPPRCState) => -cost(state.pathWeight, state.pathTime))
    val fringe = new mutable.PriorityQueue[SPPRCState]()(ord)
    explored.clear() // Avoids rebuilding explored on each call
    fringe += init

    while(fringe.nonEmpty) {
      val state = fringe.dequeue()
      val n = state.node
      if(n == to)
        return state
      explored += n
      for ((m, (w, t)) <- successors(n))
        if(!explored.contains(m))
          fringe enqueue SPPRCState(m, state.pathWeight+w, state.pathTime+t)
    }

    throw new IllegalArgumentException("Can't reach node " + to + " from node " + init.node)
  }

}

object UndirectedStaticGraph {
  object Types {
    type Node = Int
    type Weight = Int
    type Time = Int
    type Cost = Float
    type Edge = (Weight,Time)
  }

  def apply(V: Int, edgesList: List[(Types.Node,Types.Node,Types.Weight,Types.Time)]) = {
    new UndirectedStaticGraph(V, edgesList)
  }
}

