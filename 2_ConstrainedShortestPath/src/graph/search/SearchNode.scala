package graph.search

class SearchNode[State](private val parent: Option[SearchNode[State]], val state: State) {

  def pathToRoot: List[State] = {
    def pathHelper(o: Option[SearchNode[State]], xs: List[State]): List[State] = {
      o match {
        case None => xs
        case Some(p) => pathHelper(p.parent, p.state :: xs)
      }
    }
    pathHelper(parent, state :: Nil)
  }

}