package knapsack.solver

import knapsack.KnapsackTypes._
import knapsack.reversible.{ReversibleContext, ReversibleInt}

import scala.annotation.tailrec

/**
 * @author Francois Robinet
 */
class KnapsackProblem(private val items: Array[Item], private val C: Int)
                     (implicit private val context: ReversibleContext) {

  // Problem data
  private[this] val n = items.length
  private[this] val (values, weights) = items.unzip

  // Search variables
  private[this] val decided = ReversibleInt(0)
  private[this] val selected = Array.fill(n)(ReversibleInt(-1))

  // Values tracked during the search
  private[this] val v = ReversibleInt(0) // Sum of the values of selected items
  private[this] val w = ReversibleInt(0) // Sum of the weights of selected items

  // Values global for the whole search
  var bestSolution = Array.fill(n)(0)
  var bestObjective = 0

  // Used to compute the current linear relaxation bound
  private def computeLinearBound: Int = {
    var bound = v.value
    var remaining = C - w.value

    @tailrec def helper(i: Int): Int = {
      if(i < n && remaining > 0) {
        if (remaining - weights(i) >= 0) {
          remaining -= weights(i)
          bound += values(i)
          helper(i+1)
        } else {
          bound + (values(i) * remaining.toFloat / weights(i)).toInt
        }
      }
      else
        bound
    }

    helper(decided.value)
  }


  def select(i: Int): Unit = {
    decided += 1
    selected(i) := 1
    v += values(i)
    w += weights(i)
  }

  def remove(i: Int) = {
    decided += 1
    selected(i) := 0
  }

  def reject() = w.value > C || computeLinearBound <= bestObjective

  def accept() = decided.value == n && v.value > bestObjective

  def children(): Seq[() => Unit] = {
    val i = decided.value
    if(i == n) Seq()
    else {
      val left = () => select(i)
      val right = () => remove(i)
      Seq(left, right)
    }
  }

  def output(): Unit = {
    bestObjective = v.value
    bestSolution = selected.map(_.value)
  }

}
