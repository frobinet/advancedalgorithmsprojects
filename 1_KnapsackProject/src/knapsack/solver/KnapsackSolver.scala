package knapsack.solver

import java.io.{FileReader, BufferedReader}
import java.util.Scanner

import knapsack.reversible.ReversibleContext
import knapsack.KnapsackTypes.Item

import scala.collection.mutable


class KnapsackSolver(val unsortedItems: Array[Item], val C: Int) {

  val unsortedItemsWithIndices = unsortedItems.indices.map(i => (unsortedItems(i)._1, unsortedItems(i)._2, i)).toArray
  val sortedItemsWithIndices = unsortedItemsWithIndices sortWith { case((va: Int, wa: Int, _), (vb: Int, wb: Int, _)) => {
    // Compute ratios
    val ra = va.toFloat / wa
    val rb = vb.toFloat / wb

    // If same ratio
    if(ra == rb) wa > wb // take heaviest item first

    // Different ratios, just take the best one first
    else ra > rb
  }}

  val items = sortedItemsWithIndices.map { item => (item._1, item._2) }
  val context = new ReversibleContext
  val KP = new KnapsackProblem(items, C)(context)

  // Solves the Knapsack problem
  // returning the best solution and the corresponding objective
  def solve(): (Array[Int], Long) = {
    // Keep track of old indices
    val mapping = sortedItemsWithIndices.zipWithIndex.map({ case((_, _, i:Int), j:Int) => j -> i }).toMap

    // Solve the problem
    backtrackSearch()

    // Restore old indices
    val orderedSolution = Array.ofDim[Int](items.length)
    for(i <- items.indices)
      orderedSolution(mapping(i)) = KP.bestSolution(i)

    (orderedSolution, KP.bestObjective)
  }

  def backtrackSearch(): Unit = {
    if(KP.reject()) return
    if(KP.accept()) KP.output()
    for(child <- KP.children()) {
      context.pushState()
      child()
      backtrackSearch()
      context.pop()
    }
  }

  def bestItemsComparator(a: (Int,Int,Int), b: (Int,Int,Int)) = {
    val (va, wa, _) = a
    val (vb, wb, _) = b

    // Compute ratios
    val ra = va.toFloat / wa
    val rb = vb.toFloat / wb

    // If same ratio
    if(ra == rb) wa > wb // take heaviest item first

    // Different ratios, just take the best one first
    else ra > rb
  }
}

object KnapsackSolver extends App {

  // Read input file
  val filename = if(args.length > 0) args(0) else "knapsackC"
  val content = mutable.Queue[Int]()
  val sc = new Scanner(new BufferedReader(new FileReader(filename)))
  while(sc.hasNext) {
    try {
      content += sc.next.toInt
    } catch {
      case exception: NumberFormatException => exception.printStackTrace()
    }
  }
  sc.close()

  // Parse input
  val n = content.dequeue()
  val C = content.dequeue()
  val items = Array.ofDim[Item](n)
  for(i <- 0 until n)
    items(i) = (content.dequeue(), content.dequeue())

  // Solve the problem
  val solver = new KnapsackSolver(items, C)
  val (solution, objective) = solver.solve()

  // Write solution
  println(objective)
  println(solution mkString " ")
}

