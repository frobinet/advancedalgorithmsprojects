package knapsack.reversible

/*
 * @author Pierre Schaus pschaus@gmail.com
 */
class ReversibleContext() {
  private var magicNumber: Long = 0

  import scala.collection.mutable.Stack
  private val trailStack: Stack[TrailEntry] = new Stack()
  private val pointerStack: Stack[TrailEntry] = new Stack()

  /** Returns the magic number of the context */
  def magic: Long = magicNumber

  def pushOnTrail[T](reversible: ReversibleInt, value: Int): Unit = {
    val entry = new TrailEntry(reversible, value)
    trailStack.push(entry)
  }

  /** Stores the current state of the node on a stack */
  def pushState(): Unit = {
    magicNumber += 1
    pointerStack.push(trailStack.top)
  }

  /** Restores state on top od the stack of states and remove it from the stack */
  def pop(): Unit = {
    restoreUntil(pointerStack.pop())
    magicNumber += 1
  }

  def restoreUntil(until: TrailEntry): Unit = {
    while (trailStack.top != until) {
      val entry = trailStack.pop()
      entry.restore()
    }
  }

  /**
   * Restore the node to its initial state
   * Note : does not execute the on pop actions
   */
  def popAll(): Unit = {
    if(pointerStack.nonEmpty){
      restoreUntil(pointerStack.last)
    }
    // increments the magic because we want to trail again
    magicNumber += 1
  }
}
