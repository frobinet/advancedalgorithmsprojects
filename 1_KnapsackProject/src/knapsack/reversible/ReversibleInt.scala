package knapsack.reversible

/**
 * @author Pierre Schaus pschaus@gmail.com
 */
class ReversibleInt(context: ReversibleContext, v: Int) {

  // Refernece on the current value
  protected var pointer: Int = v
  private  var lastMagic: Long = -1L

  trail()

  def trail() {
    val contextMagic = context.magic
    if(lastMagic != contextMagic){
      lastMagic = contextMagic
      context.pushOnTrail(this, value)
    }
  }

  def value_=(value: Int): Unit = {
    if(value != pointer){
      trail()
      this.pointer = value
    }
  }

  @inline def :=(v: Int) = value_=(v)
  @inline def +=(v: Int) = value_=(pointer + v)
  @inline def -=(v: Int) = value_=(pointer - v)

  def value = pointer

  def restore(value: Int) : Unit = pointer = value

  override def toString(): String  = pointer.toString

}

class TrailEntry(reversible: ReversibleInt, value: Int){
  def restore(): Unit = reversible.restore(value)
}

object ReversibleInt {
  def apply(value: Int)(implicit context: ReversibleContext): ReversibleInt = {
    new ReversibleInt(context, value)
  }
}